from xbee import XBee,ZigBee
import serial

ser = serial.Serial('COM6', 9600)
xbee = ZigBee(ser)

while True:
    try:
        response = xbee.wait_read_frame()
        a = str(response).split("b'")
        #print(response)
        b = a[4].split("'")
        print(b[0])
    except KeyboardInterrupt:
        break

ser.close()